from ldap3 import Server, Connection, ALL

def Anonyme():
    server = Server('127.0.0.1:10389',get_info=ALL)
    conn = Connection(server)
    print(conn.bind())
    infoserv = server.info
    print (infoserv)

def Admin():
    server = Server('127.0.0.1:10389',get_info=ALL)
    conn = Connection(Server,user,"uid=admin", password="secret")
    print(conn.bind())
    who = conn.extend.standard.who_am_i()
    print(who)
    server.info
    print(server.info)

def Multisearch():
    server = Server("127.0.0.1:10389")
    conn = Connection(server, user="uid=admin,ou=system", password="secret")
    conn.bind()
    conn.add('uid=sheiku,ou=SISR,ou=etudiant,ou=people,dc=sio-hautil,dc=eu', 'inetOrgPerson', {'cn':'Aurélien LARIVIERRE','sn':'Ades','ou':'Parti' })
        
    print(conn.result)

def ModifPersonne():
    server = Server("127.0.0.1:10389")
    conn = Connection(server, user="uid=admin,ou=system", password="secret")
    conn.bind()
    conn.modify('uid=sheiku,ou=SISR,ou=etudiant,ou=people,dc=sio-hautil,dc=eu', {'cn': [(MODIFY_ADD, ['LARIVIERRE'])]})
    print(conn.result)

def DeletePersonne():
    server = Server("127.0.0.1:10389")
    conn = Connection(server, user="uid=admin,ou=system", password="secret")
    conn.bind()
    conn.delete('uid=sheiku,ou=SISR,ou=etudiant,ou=people,dc=sio-hautil,dc=eu')
    print(conn.result)

